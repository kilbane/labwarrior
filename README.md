# Labwarrior
Labwarrior is a script that lets you create an issue for a project on GitLab,
and at the same time create a task in Taskwarrior.

## Dependencies
- A GitLab account
- Taskwarrior
- curl
- jq
- mktemp
- sed
- echo

## Usage
1. Create a GitLab API token with read and write permissons.
2. Add your GitLab username and API token to `config.json`.
3. Run `./labwarrior.sh <name>`. `<name>` is the gitlab project name as it appears in the URL.
4. Your text editor will open. Enter the title of the issue on the first line, and the description over the remaining lines.
5. Save and exit the text editor.
6. A GitLab issue and a taskwarrior task will be created.

