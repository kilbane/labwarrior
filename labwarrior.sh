#!/bin/sh

read_config() {
  config=~/.config/labwarrior/config.json
  gitlab_username=$(jq -r .gitlab_username $config)
  gitlab_token=$(jq -r .gitlab_token $config)
}

create_gitlab_issue() {
  curl -o /dev/null --silent --request POST \
    --header "PRIVATE-TOKEN: ${gitlab_token}" \
    --data-urlencode "title=${title}" \
    --data-urlencode "description=${description}" \
    "https://gitlab.com/api/v4/projects/${encoded_project_url}/issues"
}

get_gitlab_project() {
  curl --silent --request GET \
    --header "PRIVATE-TOKEN: ${token}" \
    "https://gitlab.com/api/v4/projects/${encoded_project_url}"
}

create_taskwarrior_task() {
  task add "$title\n$description" proj:"'"$project_name"'"
}

write_description() {
  description_file=$(mktemp)
  $EDITOR $description_file
  title=$(head -n 1 $description_file)
  description=$(tail -n +2 $description_file)
}

read_config
project_url="$gitlab_username/$1"
encoded_project_url=$(echo ${project_url} | sed 's/\//%2f/g')
project_name=$(get_gitlab_project | jq -r .name)
write_description
create_gitlab_issue
create_taskwarrior_task
